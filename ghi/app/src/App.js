
import Nav from './Nav';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm'
import PresentationForm from './PresentationForm'
import MainPage from './MainPage'


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />} />
        </Route>
        <Route path="locations">
        <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path="attendees">
          <Route index element={<AttendeesList attendees={props.attendees} />} />
          <Route path="new" element={<AttendConferenceForm />} />
        </Route>
        <Route path="presentations">
        <Route path="new" element={<PresentationForm />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;








// NOTES:
// AttendeesList component is rendered: React will create and display the DOM elements
//   defined in the AttendeesList component based on the data provided in the attendees prop.
// This prop is expected to be an array of objects representing attendees.

// The rendering process is what makes the UI dynamic
//   and responsive to changes in state or props.

//including the AttendeesList component in the JSX of the App component
//     and passing data to it through a prop named attendees.

//In React, data can be passed from a parent component to a child component using props.

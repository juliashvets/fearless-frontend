function AttendeesList(props) {
    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Conference</th>
            </tr>
          </thead>
          <tbody>
            {/* the 'attendees' prop is accessed and mapped over to generate rows */}
            {props.attendees.map(attendee => {
             return (
              <tr key={attendee.href}>
                <td>{ attendee.name }</td>
                <td>{ attendee.conference }</td>
              </tr>
             )
            })}
          </tbody>
        </table>
    )
}

export default AttendeesList;


// NOTES:
//the props parameter represents the properties (props) passed to the component.
//    it's expecting a prop named attendees

//The map function is used to iterate over each element in the attendees array
//      (received as props.attendees),
//  generating a set of JSX elements for each attendee to be rendered in the table.

function createCard(name, description, pictureUrl, date, date2, location) {
    return `

      <div class="col">
      <div class="card">
        <div class="shadow bg-body rounded"></div>
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-1 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            <small class="text-muted">${date} - ${date2}</small>
        </div>
      </div>
    `;
  }
// function returns a string that represents the HTML structure
//    of a Bootstrap card. The values of the parameters are dynamically
//     inserted into the HTML using template literals (${...}).

window.addEventListener('DOMContentLoaded', async () => {

// adds an event listener to the DOMContentLoaded event,
// which fires when the HTML document has been completely loaded and parsed.

    const url = 'http://localhost:8000/api/conferences/';
//declares a constant variable url
// and assigns it the value of the API endpoint for conferences.
    try {
      const response = await fetch(url);
//uses the fetch function to make a network request to the specified API endpoint (url). The await keyword is used because fetch returns a Promise,
//and await is used in an async function to wait for the Promise to resolve.
      if (!response.ok) {
        throw new Error('Response is not ok');
//if the response from the API is not okay (status code other than 200-299).
// If it's not okay, an error is thrown.
      } else {
        const data = await response.json();
//If the response is okay, this line extracts the JSON data
//     from the response using await response.json().
// The data contains information about conferences.
      for (let conference of data.conferences) {
//initiates a loop through each conference in the data.conferences array.

        const detailUrl = `http://localhost:8000${conference.href}`;
//constructs the URL for detailed information
//about a specific conference based on the conference.href value.
        const detailResponse = await fetch(detailUrl);
//fetches detailed information about a specific conference using the constructed URL.
        if (detailResponse.ok) {
        const details = await detailResponse.json();
//If the detailed response is okay, it extracts the JSON data from the response,
//  containing information about a specific conference.
        const title = details.conference.name;
        const description = details.conference.description;
        const location = details.conference.location.name;
        const pictureUrl = details.conference.location.picture_url;
        const date = new Date(details.conference.starts).toLocaleDateString();
        const date2 = new Date(details.conference.ends).toLocaleDateString();
        const html = createCard(title, description, pictureUrl, date, date2, location);
//calls the createCard function with the extracted
//  information to generate the HTML structure for a card.
        const column = document.querySelector('.col');
        column.innerHTML += html;;
        }
//  selects the first element with the class .col in the document
//       and appends the generated HTML (html) to its existing content.
//  This is done for each conference,
//        effectively adding a new card for each conference to the same column.
      }

        }

    } catch (error) {
      console.error('error', error);
    }

  });

  // a try-catch block. Code within the try block is executed,
  // and if an error occurs, it is caught and handled in the catch block.
